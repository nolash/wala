#!/usr/bin/perl

use v5.34;

use strict;
use warnings;

# Standard imports
use File::Find qw/ find /;
use File::Spec qw/ rel2abs /;
use File::Temp qw/ tempfile /;
use Getopt::Long qw(:config auto_help);
use Digest::SHA qw / sha256_hex /;

# External imports
use Config::Simple qw/ import_from /;

our $VERSION = "0.0.1";

my $o_init = '0';
my $o_help;
my $o_config = '';
my $o_outfile = '';
my $o_binary;
my $o_force;

GetOptions(
	'h|help' => \$o_help,
	'init:s' => \$o_init,
	'c|config=s' => \$o_config,
	'o|output=s' => \$o_outfile,
	'b|binary' => \$o_binary,
	'f|force' => \$o_force,
);

if ($o_help) {
	&help();
	exit 0;
}

if ($o_init ne "0") {
	&template($o_init);
	exit 0;
}

our %config;
if ($o_config) {
	die "$o_config is not a file" unless -f $o_config;
	Config::Simple->import_from($o_config, \%config);
}

my $wd = '.';
if ($#ARGV > -1) {
	$wd = $ARGV[0];
}

sub process_license {
	my $m = $config{'license.file'};
	$_ =~ /^$m$/ || return undef;
	return $File::Find::name;
}

sub process_source {
	my $m = $config{'content.extensions'};
	$_ =~ /^.*\.$m$/ || return undef;
	return $File::Find::name;
}

our ($t_fh, $t_fn) = tempfile();

sub process_dir {
	-d $_ && return 1;
	my @routines = (
		\&process_source,
		\&process_license,
	);
	my $v;

	foreach my $r (@routines) {
		$v = &$r($_);
		if ($v) {
			my $h = Digest::SHA->new(256);
			$h->addfile($v);
			my $z = $h->hexdigest();
			print $t_fh $z . "\t" . $v . "\n";
			return;
		}
	}
}

my $d = find(
	{
		wanted => \&process_dir,
	},
	$wd,
	);

seek($t_fh, 0, 0);
my @zs;
while (<$t_fh>) {
	my ($z, $fn) = split("\t", $_);
	push(@zs, $z);
}

my $h = Digest::SHA->new(256);
foreach my $z (sort(@zs)) {
	my $b = pack("H32", $z);
	$h->add_bits($b, 256);
}
my $zz;
if ($o_binary) {
	$zz = $h->digest();
} else {
	$zz = $h->hexdigest();
}

if ($o_outfile ne '') {
        if (! $o_force && -f $o_outfile) {
		die "output file already exists: " . $o_outfile;
	}
	my $fh;
	open($fh, '>', $o_outfile);
	print $fh $zz;
	close($fh);
} else {
	print $zz;
}

close($t_fh);


sub template {
	my %template_flavor = (
		c => '(c|h)',
		perl => '(pl|pm)',
	);

	$_ = shift;

	my $exts = '';
	if (exists($template_flavor{$_})) {
		$exts = $template_flavor{$_};
	}
	print qq{[license]
file = LICENSE
#warranty = WARRANTY
#waiver = WAIVER

[content]
extensions = } . $exts . qq{
#include =
#exclude = 

[extensions]
pgp = 1
#secp256k1 = 0
};
}

sub help {
	print qq{-h, --help - Display this help
--version - Display version
-vv, -v - (Very) verbose output
--init [flavor] - Output configuration template for code flavor
-f, --force - Allow overwriting of existing files
-o, --output - Write output to file
-b, --binary - Generate binary output
};
}
